import math

radius = int(input("Type the radius: "))
print("Your radius %d, area = %s - old formatting" % (radius, (radius*radius * math.pi)))
print('area = {num} - new variant '.format \
          (num= math.pi * radius * radius))