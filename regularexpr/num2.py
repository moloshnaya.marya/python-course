import re

with open(r'D:\pycharm\works\python-course\regularexpr\txtfiles\classes_2_regexps_data_student_essays.txt', 'r') as file:
  text = file.read()

mistakes = re.findall(r"(?i)(can|could|shall|might|may|must|need|have to|has to)\s\bto\b", text)
print(mistakes)