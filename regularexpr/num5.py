import re

with open(r'D:\pycharm\works\python-course\regularexpr\txtfiles\classes_2_regexps_data_format-date.txt', 'r') as file:
  text = file.read()

pat = re.sub(r'([1-2][0-9][0-9][0-9])/([0-3][0-9])/([0][0-9]|[1][0-2])', '\\2/\\3/\\1', text)

print(pat)


