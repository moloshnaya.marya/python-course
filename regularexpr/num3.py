import re

with open(r'D:\pycharm\works\python-course\regularexpr\txtfiles\classes_2_regexps_data_student_essays.txt', 'r') as file:
  text = file.read()

mistakes = re.findall(r"(?i)(\d) (am|pm)", text)
print(mistakes)