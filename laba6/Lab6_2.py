
list1 = [i for i in range(11) if i % 2 == 0]
list2 = [i for i in range(11) if i % 2 != 0]
print('even list ', list1)
print('odd list ', list2)
d = dict.fromkeys(list1)
d.update(zip(list1, list2))
print('Two lists together in dictionary ', d)
