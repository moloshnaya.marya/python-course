
def slova():
    word = input('Type the sentence, more than 7 words:  ')
    d = {k: len(k) for k in word.split()}
    if len(word) > 7:
        print(d)
        print(sorted(d.values(), reverse=True))
    elif len(word) < 7:
        print('Not enough words to continue')
    print('Done')


slova()